package com.reecreate.swoosh.Controller

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.reecreate.swoosh.Model.Player
import com.reecreate.swoosh.R
import com.reecreate.swoosh.Utilities.EXTRA_PLAYER

import kotlinx.android.synthetic.main.activity_finish.*

class FinishActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish)

        val player = intent.getParcelableExtra<Player>(EXTRA_PLAYER)


        searchLeaguesTxt.text = "Looking for a ${player.league} ${player.skill} near you..."
    }
}
